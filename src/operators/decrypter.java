package operators;
import javax.crypto.*;
import java.security.*;
import java.nio.file.*;
import java.io.*;
import java.util.Map;
import java.util.HashMap;

public class decrypter
{
    private static String passwordFromFile;
    private static final String suffix = "fiLeeNcrypto";
    public static void newLogin(String password) throws IOException //For New Logins.
    {
        String enteredPass = password;
        FileOutputStream storeKey = new FileOutputStream("~/$HOME/.Encrypto/dekry.pt");
        String hashedValue = hasher(enteredPass+suffix);
        storeKey.write(hashedValue.getBytes());
        storeKey.close();
    }
    public static boolean isLoginSuccess (String password) throws IOException //Login for ExistingUser
    {
        FileInputStream fromStoredKey = new FileInputStream("~/$HOME/.Encrypto/dekry.pt");
        BufferedReader getKey = new BufferedReader(new InputStreamReader(fromStoredKey));
        String fetchedPass;
        while ((fetchedPass = getKey.readLine()) != null) {
            passwordFromFile = fetchedPass;
        }
        getKey.close();
        String enteredPass = hasher(password+suffix);
        return enteredPass.equals(passwordFromFile);
    }

    private static String hasher(String password) //Used to hash Password
    {
        StringBuilder hashedValue = new StringBuilder();
        try
        {
            MessageDigest sha = MessageDigest.getInstance("SHA-512");
            byte[] hashedBytes = sha.digest(password.getBytes());
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                    'a', 'b', 'c', 'd', 'e', 'f' };
            for (int idx = 0; idx < hashedBytes.length; ++idx) {
                byte b = hashedBytes[idx];
                hashedValue.append(digits[(b & 0xf0) >> 12]);
                hashedValue.append(digits[b & 0x0f]);
            }
        }catch (NoSuchAlgorithmException error){System.out.println("error in hasher.");}
        return hashedValue.toString();
    }
}
